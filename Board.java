public class Board{
	private Die d1;
	private Die d2;
	private boolean[] closedTiles;
	
	//Construstor
	public Board(){
		this.d1 = new Die();
		this.d2 = new Die();
		this.closedTiles = new boolean[12];
	}
	
	//toString() method
	public String toString(){
		String tiles = "";
		for(int i = 0; i<this.closedTiles.length; i++){
			if(this.closedTiles[i] == false){
				tiles += i+1+" ";
			}else{
				tiles += "X ";
			}
		}
		return "The tiles representation : " + tiles;
	}
	
	//playATurn() method
	public boolean playATurn(){
		this.d1.roll();
		this.d2.roll();
		System.out.println(d1);
		System.out.println(d2);
		
		int sum = d1.getPips() + d2.getPips();
		if(this.closedTiles[sum-1] == false){
			this.closedTiles[sum-1] = true;
			System.out.println("Closing tile: "+sum);
			return false;
		}else{
			System.out.println("The tile at this postion("+sum+") is already shut");
			return true;
		}
	}
}