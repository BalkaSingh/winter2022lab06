import java.util.Random;
public class Die{
	private int pips;
	private Random rd;
	
	//Constructor
	public Die(){
		this.pips = 1;
		this.rd = new Random();
	}
		
	//getter methods
	public int getPips(){
		return this.pips;
	}
	
	public Random getRd(){
		return this.rd;
	}
	
	//roll() method
	public void roll(){
		this.pips=rd.nextInt(6)+1;
	}
	
	//toString() method
	public String toString(){
		return  "The value of the pips field is : "+this.pips; 
	}
	
}	
	
