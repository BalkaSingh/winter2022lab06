public class ShutTheBox{
	public static void main(String[] args){
		System.out.println("Hello To The Game");
		Board b1 = new Board();
		boolean gameOver = false;
		
		while(gameOver == false){
			System.out.println("\nPlayer 1's turn");
			System.out.println(b1);
	
			if(b1.playATurn()){
				System.out.println("\nPlayer 2 WIN");
				gameOver=true;
				continue;
			}else{
				System.out.println("\nPlayer 2's turn");
				System.out.println(b1);
			}
			
			if(b1.playATurn()){
				System.out.println("\nPlayer 1 WIN");
				gameOver=true;
				continue;
			}
		}
		
	}
}